# FEXNA

A content pack for the SRPG game engine [Tactile](http://www.bwdyeti.com/tactile) that recreates the experience of GBAFE, and simplifies making FE fangames. FEXNA also contains sample chapters and data to help get started using Tactile.

## Setup & Usage

1. Clone the repo with ```git clone --recursive https://gitlab.com/FEXNA/FEXNA-Content.git```, or after downloading run ```git submodule update --init --recursive``` to download the [FEXNA-Text](https://gitlab.com/FEXNA/FEXNA-Text) submodule.
2. Archive the contents of the repo into a zip file, and place that zip file into the `srccontent/` folder of the Tactile Core Editor.

![archive screencap](https://i.imgur.com/yCe26ve.png)

![archive contents](https://i.imgur.com/FWBGHU2.png)

3. Create a new project using the Tactile editor, and select the FEXNA content zip.

![new project form](https://i.imgur.com/ithH3Gw.png)

## Licensing

Any content from FE is (c) Nintendo and Intelligent Systems, and its use is not licensed. Use at your own risk.

Any other original graphical and audio content (c) FE7x development team. Do not distribute assets and only use for educational purposes, unless their use is licensed otherwise. All rights reserved.
